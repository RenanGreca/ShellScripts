#check for large files:
sudo du -h --max-depth=1 / | grep '[0-9]G\>' # folders larger than 1GB
sudo find / -name '*' -size +1G # files larger than 1GB
#dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -n
#gksudo nautilus /root/.local/share/Trash/files # Be sure to enable viewing of hidden files.
